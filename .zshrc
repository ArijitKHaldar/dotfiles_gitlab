#
#        ███████╗    ███████╗    ██╗  ██╗    ██████╗      ██████╗
#        ╚══███╔╝    ██╔════╝    ██║  ██║    ██╔══██╗    ██╔════╝
#          ███╔╝     ███████╗    ███████║    ██████╔╝    ██║     
#         ███╔╝      ╚════██║    ██╔══██║    ██╔══██╗    ██║     
# ██╗    ███████╗    ███████║    ██║  ██║    ██║  ██║    ╚██████╗
# ╚═╝    ╚══════╝    ╚══════╝    ╚═╝  ╚═╝    ╚═╝  ╚═╝     ╚═════╝
#
# ~/.zshrc: configuration for zsh shell
# https://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=.%20Z%20S%20H%20R%20C

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Enable colors & change prompt
autoload -U colors && colors

###########################################################################
### EXPORT
###########################################################################
export TERM="xterm-256color"                                             # getting proper colors
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"
#export EDITOR="emacsclient -t -a ''"                                    # $EDITOR use Emacs in terminal
#export VISUAL="emacsclient -c -a emacs"                                 # $VISUAL use Emacs in GUI mode
export EDITOR="vim"                                                      # $EDITOR use Vim in terminal
export VISUAL="vim"                                                      # $VISUAL use Vim in GUI mode
export ALTERNATE_EDITOR=""                                               # setting for emacsclient

###########################################################################
### History lengths to store
###########################################################################
HISTSIZE=1000
SAVEHIST=2000
if [ -f ~/.cache/zsh/history ]; then
    HISTFILE=~/.cache/zsh/history
else
    mkdir -p ~/.cache/zsh
    touch ~/.cache/zsh/history
    HISTFILE=~/.cache/zsh/history
fi

###########################################################################
### SET MANPAGER - Uncomment only one of these!
###########################################################################
### "bat" as manpager
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
### "vim" as manpager
# export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'
### "nvim" as manpager
# export MANPAGER="nvim -c 'set ft=man' -"

###########################################################################
### SET VI MODE ###
###########################################################################
# Comment this line out to enable default emacs-like bindings
#bindkey -v
#export KEYTIMEOUT=1

###########################################################################
### PROMPT
###########################################################################
# Disable these if using the Starship prompt

###########################################################################
### SETTING THE STARSHIP PROMPT ###
###########################################################################
#eval "$(starship init zsh)"

###########################################################################
### ADD TO PATH   -d checks if that directory exists (tip: use .zshenv)
###########################################################################
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/Applications" ] ;
  then PATH="$HOME/Applications:$PATH"
fi

###########################################################################
### CHANGE TITLE OF TERMINALS
###########################################################################
case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|konsole*|xfce4-terminal*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
    #PROMPT_COMMAND='echo -ne "\033]0;${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    #PROMPT_COMMAND='echo -ne "\033_${PWD/#$HOME/\~}\033\\"'
    ;;
esac

###########################################################################
### ARCHIVE EXTRACTION
###########################################################################
# usage: extract <file>
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
 else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.cbr|*.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.cbz|*.epub|*.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *.cpio)      cpio -id < ./"$n"  ;;
            *.cba|*.ace)      unace x ./"$n"      ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}
IFS=$SAVEIFS

###########################################################################
### ALIASES
###########################################################################
if [ -f ~/.config/aliases ]; then
    . ~/.config/aliases
fi

# cd back 'd' number of times with $up d
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}

###########################################################################
### BASH INSULTER ###
###########################################################################
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi

###########################################################################
### MISCELLANEOUS
###########################################################################

# set path for tty device on interactive shell
#export GPG_TTY=$(tty) # uncomment this if using tty prompt, mandatory on WSL

# start ssh agent
#eval $(ssh-agent -s)
