# default apps
export EDITOR="vim"
export TERMINAL="kitty"
export BROWSER="brave"

###########################################################################
### ADD TO PATH   -d checks if that directory exists
###########################################################################
# This function is originally in /etc/profile but is accessible only to
# scripts in /etc/profile.d
append_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

if [ -d "$HOME/.bin" ] ; then
  append_path '$HOME/.bin'
fi

if [ -d "$HOME/.local/bin" ] ; then
  append_path '$HOME/.local/bin'
fi

if [ -d "$HOME/Applications" ] ; then
  append_path '$HOME/Applications'
fi

# Force PATH to be environment
export PATH

###########################################################################
### SETTING OTHER ENVIRONMENT VARIABLES
###########################################################################
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_CONFIG_DIRS="/etc/xdg"
export LESSHISTFILE="-"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/npm/npmrc"
export N_PREFIX="$HOME/.local/bin/n" # Node.js Version management
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default" #Make the wineprefixes folder first
export XMONAD_CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/xmonad" # xmonad.hs is expected to stay here
export XMONAD_DATA_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/xmonad"
export XMONAD_CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/xmonad"
