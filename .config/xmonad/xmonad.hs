--------------------------------------
-- IMPORT PACKAGES
--------------------------------------
-- Base
import XMonad
import System.IO (hPutStrLn)
import System.Exit (exitWith, ExitCode(..))
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S
import XMonad.Prompt.ConfirmPrompt

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.WindowSwallowing

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.Cursor
import XMonad.Util.Hacks (windowedFullscreenFixEventHook, javaHack, trayerAboveXmobarEventHook, trayAbovePanelEventHook, trayerPaddingXmobarEventHook, trayPaddingXmobarEventHook, trayPaddingEventHook)

--------------------------------------
-- Variables Initialization
--------------------------------------
myFont :: String
myFont = "xft:CaskaydiaCove Nerd Font:weight=book:pixelsize=12:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask        -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "kitty"    -- Sets default terminal

myBrowser :: String
myBrowser = "brave"  -- Sets brave as browser

myFileManager :: String
myFileManager = "thunar"  -- Sets thunar as file manager

myEditor :: String
-- myEditor = "emacsclient -c -a 'emacs' "  -- Sets emacs as editor
myEditor = myTerminal ++ " -e nvim "        -- Sets neovim as editor

myBorderWidth :: Dimension
myBorderWidth = 2           -- Sets border width for windows

myNormColor :: String
myNormColor   = "#111111"   -- Border color of normal windows

myFocusColor :: String
myFocusColor  = "#FF7200"   -- Border color of focused windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

--------------------------------------
-- Autostart
--------------------------------------
myStartupHook :: X ()
myStartupHook = do
    spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &"
    spawnOnce "picom --experimental-backends &"
    -- spawnOnce "nm-applet &"
    spawnOnce "dunst --no-startup-id &"
    spawnOnce "trayer --edge top --align right --widthtype request --heighttype pixel --padding 5 --SetDockType true --SetPartialStrut false --expand true --monitor primary --transparent true --alpha 0 --tint 0x282c34  --height 22 &"
    spawnOnce "xcape -t 300 -e 'Super_L=Super_L|F1;Control_L=Control_L|F1' &"
    spawnOnce "thunar --daemon &"
       -- Needs xwallpaper to be installed
    -- spawnOnce "xargs xwallpaper --stretch < ~/Pictures/.xwallpaper"  -- set last saved with xwallpaper
    -- spawnOnce "/bin/ls ~/Pictures/wallpapers/ | shuf -n 1 | xargs xwallpaper --stretch"  -- set random xwallpaper
       -- Needs feh to be installed 
    spawnOnce "~/.fehbg &"  -- set last saved feh wallpaper
    -- spawnOnce "feh --randomize --bg-fill --no-fehbg ~/Pictures/wallpapers/*"  -- feh set random wallpaper
    -- spawnOnce "nitrogen --restore &"   -- if you prefer nitrogen to feh
    setDefaultCursor xC_left_ptr -- Set cursor theme
    setWMName "LG3D"
    spawnOnce "canberra-gtk-play -i service-login -d 'Login' &" -- add a login sound
    spawnOnce "ibus-daemon -drxR &"

--------------------------------------
-- Generating Layouts and Naming Them
--------------------------------------
--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw True (Border i i i i) True (Border i i i i) True -- Set the first True to False for all windows to have spacing

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

--------------------------------------
-- Colours for Tabbed Layout
--------------------------------------
-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

--------------------------------------
-- Show Workspace Name when changing workspace
--------------------------------------
-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:CaskaydiaCove Nerd Font:weight=book:pixelsize=60:antialias=true:hinting=true"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

--------------------------------------
-- Defining Tiling Layout Styles
--------------------------------------
-- The layout hook
myLayoutHook = smartBorders $ lessBorders OnlyFloat $ avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion

--------------------------------------
-- Workspace Names that go on XMobar
--------------------------------------
--myWorkspaces = ["home", "www", "sys", "doc", "vbox", "chat", "mus", "vid", "gfx"]
myWorkspaces = ["\xf015 ", "\xf268 ", "\xf121 ", "\xf109 ", "\xfaa2 ", "\xf001 ", "\xf03d ", "\xe7b8 ", "\xf1b0 "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
-- Install xdotool for clickable workspace to work
    where i = fromJust $ M.lookup ws myWorkspaceIndices

--------------------------------------
-- Window Rules for specific Softwares
--------------------------------------
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , stringProperty "WM_NAME" =? "File Operation Progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     , className =? "kitty"           --> doShift ( myWorkspaces !! 2 )
     , title =? "Oracle VM VirtualBox Manager"  --> doFloat
     , className =? "firefox"     --> doShift ( myWorkspaces !! 1 )
     --, className =? "Chromium"     --> doShift ( myWorkspaces !! 1 )
     , className =? "Brave-browser"   --> doShift ( myWorkspaces !! 1 ) -- $(xprop | grep WM_CLASS) to know the className
     , className =? "mpv"             --> doShift ( myWorkspaces !! 6 )
     , className =? "Gimp-2.10"            --> doShift ( myWorkspaces !! 7 )
     , className =? "VirtualBox Manager" --> doShift  ( myWorkspaces !! 4 )
     , (className =? "firefox" <&&> appName =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , isFullscreen -->  doFullFloat
     ]

--------------------------------------
-- Set Key Bindings
--------------------------------------
myKeys :: [(String, X ())]
myKeys =
    -- Xmonad
        [ ("M-C-r", spawn "xmonad --recompile")  -- Recompiles xmonad (Super + Ctrl + r)
        , ("M-S-r", spawn "xmonad --restart")    -- Restarts xmonad (Super + Shift + r)
        , ("C-q", spawn "canberra-gtk-play -i service-logout -d 'Logout'" >> confirmPrompt def "exit" (io (exitWith ExitSuccess)))              -- Quits xmonad (Ctrl + q)

    -- Run Prompt
        , ("M-S-<Return>", spawn "dmenu_run -i -p \"Run: \"") -- Dmenu (Super + Shift + Enter)

    -- Other Dmenu Prompts
    -- In Xmonad and many tiling window managers, M-p is the default keybinding to
    -- launch dmenu_run, so I've decided to use M-p plus KEY for these dmenu scripts.
        , ("M-p a", spawn "$HOME/.local/bin/playStreaming")    -- choose an ambient background (Super + p -> a)
        , ("M-p p", spawn "passmenu")     -- passmenu (Super + p -> p)

    -- Useful programs to have a keybinding for launch
        , ("M-<Return>", spawn (myTerminal)) -- (Super + Enter)
        -- , ("M-b", spawn (myBrowser ++ " https://gitlab.com/arijitkhaldar/")) -- (Super + b)
        , ("M-b", spawn (myBrowser ++ " --use-gl=desktop --enable-features=VaapiVideoDecoder,VaapiVideoEncoder")) -- (Super + b)
        , ("M-M1-h", spawn (myTerminal ++ " -e htop")) -- (Super + LAlt + h)
        , ("M-e", spawn (myFileManager)) -- (Super + e)
        , ("M-<F1>", spawn "rofi -show drun")

    -- Kill windows
        , ("M-c", kill1)     -- Kill the currently focused client (Alt + q)
        , ("M-S-c", killAll)   -- Kill all windows on current workspace (Alt + a)
    
    -- Power options
        , ("M-<End>", spawn "poweroff")
        , ("M-S-<End>", spawn "reboot")

    -- Workspaces
        , ("M-.", nextScreen)  -- Switch focus to next monitor
        , ("M-,", prevScreen)  -- Switch focus to prev monitor
        , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
        , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

    -- Floating windows
        , ("M-f", sendMessage (T.Toggle "floats")) -- Toggles my 'floats' layout
        , ("M-t", withFocused $ windows . W.sink)  -- Push floating window back to tile
        , ("M-S-t", sinkAll)                       -- Push ALL floating windows to tile

    -- Increase/decrease spacing (gaps)
        , ("C-M1-j", decWindowSpacing 4)         -- Decrease window spacing
        , ("C-M1-k", incWindowSpacing 4)         -- Increase window spacing
        , ("C-M1-h", decScreenSpacing 4)         -- Decrease screen spacing
        , ("C-M1-l", incScreenSpacing 4)         -- Increase screen spacing

    -- Windows navigation
        , ("M-m", windows W.focusMaster)  -- Move focus to the master window
        , ("M-j", windows W.focusDown)    -- Move focus to the next window
        , ("M-k", windows W.focusUp)      -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
        , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
        , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
        , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
        , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

    -- Layouts
        , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
        , ("M-<F11>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full

    -- Increase/decrease windows in the master pane or the stack
        , ("M-S-<Up>", sendMessage (IncMasterN 1))      -- Increase # of clients master pane
        , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease # of clients master pane
        , ("M-C-<Up>", increaseLimit)                   -- Increase # of windows
        , ("M-C-<Down>", decreaseLimit)                 -- Decrease # of windows

    -- Window resizing
        , ("M-h", sendMessage Shrink)                   -- Shrink horiz window width
        , ("M-l", sendMessage Expand)                   -- Expand horiz window width
        , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
        , ("M-M1-k", sendMessage MirrorExpand)          -- Expand vert window width

    -- Sublayouts
    -- This is used to push windows to tabbed sublayouts, or pull them out of it.
        , ("M-C-h", sendMessage $ pullGroup L)
        , ("M-C-l", sendMessage $ pullGroup R)
        , ("M-C-k", sendMessage $ pullGroup U)
        , ("M-C-j", sendMessage $ pullGroup D)
        , ("M-C-m", withFocused (sendMessage . MergeAll))
        -- , ("M-C-u", withFocused (sendMessage . UnMerge))
        , ("M-C-/", withFocused (sendMessage . UnMergeAll))
        , ("M-C-.", onGroup W.focusUp')    -- Switch focus to next tab
        , ("M-C-,", onGroup W.focusDown')  -- Switch focus to prev tab

    -- Set wallpaper
    --    , ("M-<F10>", spawn "sxiv -r -q -t -o ~/wallpapers/*")
    --    , ("M-<F10>", spawn "/bin/ls ~/wallpapers | shuf -n 1 | xargs xwallpaper --stretch")
        , ("M-<F10>", spawn "feh --randomize --bg-fill ~/Pictures/wallpapers/*")

    -- Controls for mocp music player (SUPER-u followed by a key)
        , ("M-u p", spawn "mocp --play")
        , ("M-u l", spawn "mocp --next")
        , ("M-u h", spawn "mocp --previous")
        , ("M-u <Space>", spawn "mocp --toggle-pause")

-- Multimedia Keys
        , ("<XF86AudioPlay>", spawn (myTerminal ++ "mocp --play"))
        , ("<XF86AudioPrev>", spawn (myTerminal ++ "mocp --previous"))
        , ("<XF86AudioNext>", spawn (myTerminal ++ "mocp --next"))
        , ("<XF86AudioMute>", spawn "$HOME/.local/bin/volumeChange mute")
        , ("<XF86AudioLowerVolume>", spawn "$HOME/.local/bin/volumeChange down")
        , ("<XF86AudioRaiseVolume>", spawn "$HOME/.local/bin/volumeChange up")
        , ("<XF86MonBrightnessUp>", spawn "$HOME/.local/bin/brightnessChange up")
        , ("<XF86MonBrightnessDown>", spawn "$HOME/.local/bin/brightnessChange down")
        , ("<XF86HomePage>", spawn (myBrowser ++ "https://"))
        , ("<XF86Search>", spawn "dmsearch")
        , ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
        , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
        , ("<XF86Eject>", spawn "toggleeject")
        , ("<Print>", spawn "dmscrot")
        ]
    -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))

--------------------------------------
-- Main Method
--------------------------------------
main :: IO ()
main = do
    -- Launching three instances of xmobar on their monitors.
    
    xmprocleft <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc0.hs"
    -- xmprocmiddle <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/xmobarrc1.hs"
    -- xmprocright <- spawnPipe "xmobar -x 2 $HOME/.config/xmobar/xmobarrc2.hs"
    xmonad $ docks $ ewmhFullscreen $ ewmh $ def
        { manageHook         = myManageHook <+> manageDocks
        , handleEventHook    = windowedFullscreenFixEventHook <> swallowEventHook (className =? "Alacritty"  <||> className =? "kitty" <||> className =? "XTerm") (return True) <> trayerPaddingXmobarEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = showWName' myShowWNameTheme $ myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        , logHook = dynamicLogWithPP $ xmobarPP
              -- the following variables beginning with 'pp' are settings for xmobar.
              { ppOutput = \x -> hPutStrLn xmprocleft x                          -- xmobar on monitor 1
                           -- >> hPutStrLn xmprocmiddle x                          -- xmobar on monitor 2
                           -- >> hPutStrLn xmprocright x                          -- xmobar on monitor 3
              , ppCurrent = xmobarColor "#f8f16a" "" . wrap "<fn=1>" "</fn>"         -- Workspace that I am viewing now
              , ppVisible = xmobarColor "#98be65" "" . wrap "<fn=1>" "</fn>" . clickable              -- Workspace that is open on any monitor other than this one
              , ppHidden = xmobarColor "#2ac3de" "" . wrap "<fn=1>" "</fn>" . clickable -- Hidden workspaces that have any open software in it but not open on any monitors
              , ppHiddenNoWindows = xmobarColor "#c0caf5" "" . wrap "<fn=1>" "</fn>" . clickable     -- Workspaces with no open softwares and not open on any monitors
              , ppTitle = xmobarColor "#c0caf5" "" . shorten 60               -- Title of active window
              , ppSep =  "<fc=#444b6a> | </fc>"                    -- Separator character
              , ppUrgent = xmobarColor "#EBCB8B" "" . wrap "!<fn=1>" "</fn>!"            -- Urgent workspace
              , ppExtras  = [windowCount]                                     -- # of windows current workspace
              --, ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]                  -- name of workspaces, current layout, current title of open software, number of open windows in current workspace
              , ppOrder  = \(ws:_:_:_) -> [ws]                               -- stopped showing the current layout, number of open programs in current workspace
              }
        } `additionalKeysP` myKeys
