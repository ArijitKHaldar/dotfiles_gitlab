"
"         ██╗   ██╗    ██╗    ███╗   ███╗    ██████╗      ██████╗
"         ██║   ██║    ██║    ████╗ ████║    ██╔══██╗    ██╔════╝
"         ██║   ██║    ██║    ██╔████╔██║    ██████╔╝    ██║     
"         ╚██╗ ██╔╝    ██║    ██║╚██╔╝██║    ██╔══██╗    ██║     
"  ██╗     ╚████╔╝     ██║    ██║ ╚═╝ ██║    ██║  ██║    ╚██████╗
"  ╚═╝      ╚═══╝      ╚═╝    ╚═╝     ╚═╝    ╚═╝  ╚═╝     ╚═════╝
"
"
" https://patorjk.com/software/taag/#p=display&c=bash&f=ANSI%20Shadow&t=.%20V%20I%20M%20R%20C

" All Vim-Plug Github links goes in between the #begin() and #end()
" VIM users-
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" NEOVIM users-
" sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

set nocompatible                                       " Stop using VI mode
filetype off                                           " Turn off filetype temporarily

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"    => Vim-Plug For Managing Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin('~/.vim/plugged')
"{{ Theme-ing stuff }}
    Plug 'itchyny/lightline.vim'                       " Lightline status bar
"{{ Syntax Highlighting and Colors }}
    Plug 'vim-python/python-syntax'                    " Python highlighting
    Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' } " Enable coloring of hexadecimal colors (needs Golang)
"{{ Junegunn Choi Plugins }}
    Plug 'junegunn/vim-emoji'                          " Vim needs emojis!
"{{ File management }}
    Plug 'vifm/vifm.vim'                               " Vifm
    Plug 'scrooloose/nerdtree'                         " Nerdtree
    Plug 'tiagofumo/vim-nerdtree-syntax-highlight'     " Highlighting Nerdtree
    Plug 'ryanoasis/vim-devicons'                      " Icons for Nerdtree
call plug#end()

filetype plugin indent on                              " Set filetype, plugin and indent

" Brief help
" :PlugStatus       - check status of plugins
" :PlugInstall      - installs plugins
" :PlugClean        - Remove unlisted plugins; append `!` to auto-approve removal
" :PlugUpdate       - Install or update plugins
" :PlugUpgrade      - Upgrade vim-plug itself

" Put your non-Plugin stuff after this line

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set path+=**                                           " Searches current directory recursively   
set wildmenu
set incsearch                                          " Incremental search
set nowrap
set scrolloff=8                                        " Keeps cursor relatively centered in a long file
"set hidden                                            " Needed to keep multiple buffers open
"set nobackup                                          " No auto backups
"set noswapfile                                        " No swap
if !has('gui_running')
    set t_Co=256                                       " If terminal supports 256 colors, enable it
endif
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
set number relativenumber                              " Display line numbers
set clipboard=unnamedplus                              " Copy/paste between vim and other programs
syntax enable                                          " Enable syntax highlighting detection
let g:rehash256 = 1                                    " Make 256 colors as close to default dark theme as possible

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

":imap ii <Esc>                                        " Remap ESC to ii

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Theming
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set background=dark                                    " For the dark mode
if exists('+termguicolors')                            " If TrueColor available on the terminal, enable color highlighting
    let g:Hexokinase_highlighters = [ 'backgroundfull' ]
    autocmd VimEnter * HexokinaseTurnOn
endif
set cursorline cursorlineopt=number                    " Make current line number vivid
highlight CursorLineNr     ctermfg=7    guifg=#FFF570    ctermbg=8  guibg=#2B2B2B   cterm=NONE gui=NONE
highlight LineNr           ctermfg=8    guifg=#666666    ctermbg=NONE    guibg=NONE  cterm=NONE  gui=NONE
highlight VertSplit        ctermfg=0    ctermbg=8       cterm=NONE
highlight Statement        ctermfg=2    ctermbg=NONE    cterm=NONE
highlight Directory        ctermfg=4    ctermbg=NONE    cterm=NONE
highlight StatusLine       ctermfg=7    ctermbg=8       cterm=NONE
highlight StatusLineNC     ctermfg=7    ctermbg=8       cterm=NONE
highlight NERDTreeClosable ctermfg=2
highlight NERDTreeOpenable ctermfg=8
highlight Comment          ctermfg=4    ctermbg=NONE    cterm=italic
highlight Constant         ctermfg=12   ctermbg=NONE    cterm=NONE
highlight Special          ctermfg=4    ctermbg=NONE    cterm=NONE
highlight Identifier       ctermfg=6    ctermbg=NONE    cterm=NONE
highlight PreProc          ctermfg=5    ctermbg=NONE    cterm=NONE
highlight String           ctermfg=12   ctermbg=NONE    cterm=NONE
highlight Number           ctermfg=1    ctermbg=NONE    cterm=NONE
highlight Function         ctermfg=1    ctermbg=NONE    cterm=NONE
" highlight WildMenu         ctermfg=0       ctermbg=80      cterm=NONE
" highlight Folded           ctermfg=103     ctermbg=234     cterm=NONE
" highlight FoldColumn       ctermfg=103     ctermbg=234     cterm=NONE
" highlight DiffAdd          ctermfg=NONE    ctermbg=23      cterm=NONE
" highlight DiffChange       ctermfg=NONE    ctermbg=56      cterm=NONE
" highlight DiffDelete       ctermfg=168     ctermbg=96      cterm=NONE
" highlight DiffText         ctermfg=0       ctermbg=80      cterm=NONE
" highlight SignColumn       ctermfg=244     ctermbg=235     cterm=NONE
" highlight Conceal          ctermfg=251     ctermbg=NONE    cterm=NONE
" highlight SpellBad         ctermfg=168     ctermbg=NONE    cterm=underline
" highlight SpellCap         ctermfg=80      ctermbg=NONE    cterm=underline
" highlight SpellRare        ctermfg=121     ctermbg=NONE    cterm=underline
" highlight SpellLocal       ctermfg=186     ctermbg=NONE    cterm=underline
" highlight Pmenu            ctermfg=251     ctermbg=234     cterm=NONE
" highlight PmenuSel         ctermfg=0       ctermbg=111     cterm=NONE
" highlight PmenuSbar        ctermfg=206     ctermbg=235     cterm=NONE
" highlight PmenuThumb       ctermfg=235     ctermbg=206     cterm=NONE
" highlight TabLine          ctermfg=244     ctermbg=234     cterm=NONE
" highlight TablineSel       ctermfg=0       ctermbg=247     cterm=NONE
" highlight TablineFill      ctermfg=244     ctermbg=234     cterm=NONE
" highlight CursorColumn     ctermfg=NONE    ctermbg=236     cterm=NONE
" highlight CursorLine       ctermfg=NONE    ctermbg=236     cterm=NONE
" highlight ColorColumn      ctermfg=NONE    ctermbg=236     cterm=NONE
" highlight Cursor           ctermfg=0       ctermbg=5       cterm=NONE
" highlight htmlEndTag       ctermfg=114     ctermbg=NONE    cterm=NONE
" highlight xmlEndTag        ctermfg=114     ctermbg=NONE    cterm=NONE

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status Line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set laststatus=2                                       " Make lightline always show statusbar
set cmdheight=2                                        " Command area below the statusbar
"set noshowmode                                        " Stop non-normal modes to show below statusbar 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set expandtab                                          " Use spaces instead of tabs
set shiftwidth=4 softtabstop=4                         " Required for expandtab

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERDTree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '►'
let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize=38

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vifm
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <Leader>vv :Vifm<CR>
map <Leader>vs :VsplitVifm<CR>
map <Leader>sp :SplitVifm<CR>
map <Leader>dv :DiffVifm<CR>
map <Leader>tv :TabVifm<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Open terminal inside Vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <Leader>tt :vnew term://bash<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Mouse Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set mouse=a     " All
"set mouse=nicr  " Normal Insert Command-line Return

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set splitbelow splitright

" Remap splits navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" Removes pipes | that act as seperators on splits
set fillchars+=vert:\ 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spelling and text managing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"set hlsearch                                           " Highlight all matches
set encoding=UTF-8
"set autoread                                          " Autoupdate text to and from buffer when changed out of the present instance
"set nospell
set confirm                                            " Ask when closing an unsaved file

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Other Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:python_highlight_all = 1
"let g:one_allow_italics = 1                            " I love italic for comments

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => My Keybindings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"map <F10> :setlocal spell spelllang = en_us<CR>        " Enable english spelling checking
