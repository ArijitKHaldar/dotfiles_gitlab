#         ██████╗      █████╗     ███████╗    ██╗  ██╗    ██████╗      ██████╗
#         ██╔══██╗    ██╔══██╗    ██╔════╝    ██║  ██║    ██╔══██╗    ██╔════╝
#         ██████╔╝    ███████║    ███████╗    ███████║    ██████╔╝    ██║     
#         ██╔══██╗    ██╔══██║    ╚════██║    ██╔══██║    ██╔══██╗    ██║     
#  ██╗    ██████╔╝    ██║  ██║    ███████║    ██║  ██║    ██║  ██║    ╚██████╗
#  ╚═╝    ╚═════╝     ╚═╝  ╚═╝    ╚══════╝    ╚═╝  ╚═╝    ╚═╝  ╚═╝     ╚═════╝
#                                                                             
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc) for examples
# https://patorjk.com/software/taag/#p=display&c=bash&f=ANSI%20Shadow&t=.%20B%20A%20S%20H%20R%20C

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

###########################################################################
### EXPORT
###########################################################################
export TERM="xterm-256color"                      # getting proper colors
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
#export EDITOR="emacsclient -t -a ''"             # $EDITOR use Emacs in terminal
#export VISUAL="emacsclient -c -a emacs"          # $VISUAL use Emacs in GUI mode
export EDITOR="vim"                               # $EDITOR use Vim in terminal
export VISUAL="vim"                               # $VISUAL use Vim in GUI mode
export ALTERNATE_EDITOR=""                        # setting for emacsclient

###########################################################################
# History lengths to store
###########################################################################
HISTSIZE=1000
HISTFILESIZE=2000

###########################################################################
### SET MANPAGER - Uncomment only one of these!
###########################################################################
### "bat" as manpager
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
### "vim" as manpager
# export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'
### "nvim" as manpager
# export MANPAGER="nvim -c 'set ft=man' -"

###########################################################################
### SET VI MODE ###
###########################################################################
# Comment these lines out to enable default emacs-like bindings
#set -o vi
#bind -m vi-command 'Control-l: clear-screen'
#bind -m vi-insert 'Control-l: clear-screen'

###########################################################################
### PROMPT
###########################################################################
# Disable these if using Starship prompt
#PS1='\u@\h:\w\$ '
PS1='\[\033[01;31m\][\[\033[01;33m\]\u\[\033[01;32m\]@\[\033[01;34m\]\h\[\033[00m\]:\[\033[00;35m\]\w\[\033[01;31m\]]\[\033[00m\]\$ '

###########################################################################
### SETTING THE STARSHIP PROMPT ###
###########################################################################
## find out which distribution we are running on
#_distro=$(awk '/^ID=/' /etc/*-release | awk -F'=' '{ print tolower($2) }')
## set an icon based on the distro
#case $_distro in
#    *kali*)                  ICON="ﴣ";;
#    *arch*)                  ICON="";;
#    *debian*)                ICON="";;
#    *raspbian*)              ICON="";;
#    *ubuntu*)                ICON="";;
#    *elementary*)            ICON="";;
#    *fedora*)                ICON="";;
#    *coreos*)                ICON="";;
#    *gentoo*)                ICON="";;
#    *mageia*)                ICON="";;
#    *centos*)                ICON="";;
#    *opensuse*|*tumbleweed*) ICON="";;
#    *sabayon*)               ICON="";;
#    *slackware*)             ICON="";;
#    *linuxmint*)             ICON="";;
#    *alpine*)                ICON="";;
#    *aosc*)                  ICON="";;
#    *nixos*)                 ICON="";;
#    *devuan*)                ICON="";;
#    *manjaro*)               ICON="";;
#    *rhel*)                  ICON="";;
#    *)                       ICON="";;
#esac
#export STARSHIP_DISTRO="$ICON "
## Get the status code from the last command executed
#STATUS=$?
## Get the number of jobs running.
#NUM_JOBS=$(jobs -p | wc -l)
## Set the prompt to the output of `starship prompt`
#PS1="$(starship prompt --status=$STATUS --jobs=$NUM_JOBS)"
#eval "$(starship init bash)"

###########################################################################
### ADD TO PATH   -d checks if that directory exists
###########################################################################
# This function is originally in /etc/profile but is accessible only to
# scripts in /etc/profile.d
append_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

if [ -d "$HOME/.bin" ] ; then
    append_path '$HOME/.bin'
fi
if [ -d "$HOME/.local/bin" ] ; then
    append_path '$HOME/.local/bin'
fi
if [ -d "$HOME/Applications" ] ; then
    append_path '$HOME/Applications'
fi

if [ -d "/usr/local/MATLAB/R2018a/bin" ] ; then # This is only for MATLAB on my Acer Laptop. Comment this out
    append_path '/usr/local/MATLAB/R2018a/bin'
fi
#append_path '/home/arijit/.local/texlive/2022/bin/x86_64-linux'

# Force PATH to be environment
export PATH

###########################################################################
### SETTING OTHER ENVIRONMENT VARIABLES
###########################################################################
if [ -z "$XDG_CONFIG_HOME" ] ; then
    export XDG_CONFIG_HOME="$HOME/.config"
fi
if [ -z "$XDG_DATA_HOME" ] ; then
    export XDG_DATA_HOME="$HOME/.local/share"
fi
if [ -z "$XDG_CACHE_HOME" ] ; then
    export XDG_CACHE_HOME="$HOME/.cache"
fi
if [ -z "$CARGO_HOME" ] ; then
    export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
fi
export XMONAD_CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/xmonad" # xmonad.hs is expected to stay here
export XMONAD_DATA_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/xmonad"
export XMONAD_CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/xmonad"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
#export MANPATH="/home/arijit/.local/texlive/2022/texmf-dist/doc/man"
#export INFOPATH="/home/arijit/.local/texlive/2022/texmf-dist/doc/info"

###########################################################################
### CHANGE TITLE OF TERMINALS
###########################################################################
case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty*|st|konsole*|xfce4-terminal*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
    #PROMPT_COMMAND='echo -ne "\033]0;${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    #PROMPT_COMMAND='echo -ne "\033_${PWD/#$HOME/\~}\033\\"'
    ;;
esac

###########################################################################
### SHOPT -s for set, -u for unset
###########################################################################
shopt -s autocd         # Change directory without typing cd
shopt -s cdspell        # Autocorrect misspellings of directory
shopt -s cmdhist        # Save multiline command history as single line
shopt -s histappend     # Don't over-write history
shopt -s dotglob        # Match hidden files too
shopt -s globstar       # (**)Match all files, (**/)directories and subdirectories
#shopt -s expand_aliases # Expand aliases even on non-interactive shell
shopt -s checkwinsize   # Refresh terminal size after each command
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

###########################################################################
### ARCHIVE EXTRACTION
###########################################################################
# usage: extract <file>
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
 else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.cbr|*.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.cbz|*.epub|*.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *.cpio)      cpio -id < ./"$n"  ;;
            *.cba|*.ace)      unace x ./"$n"      ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}
IFS=$SAVEIFS

###########################################################################
### ALIASES
###########################################################################
if [ -f ~/.config/aliases ]; then
    . ~/.config/aliases
fi

# cd back 'd' number of times with $up d
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}

###########################################################################
### BASH INSULTER ###
###########################################################################
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi

###########################################################################
### MISCELLANEOUS
###########################################################################
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# Requires lesspipe package to be installed

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# set path for tty device on interactive shell
#export GPG_TTY=$(tty)             # uncomment this if using tty prompt, mandatory on WSL

# start ssh agent
#eval $(ssh-agent -s)


# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
