# My Linux Configuration Files

This is my personal repo to store my linux dotfiles.

![My Desktop](./Documents/Screenshots/Screenshot_2022-07-29_12-28-41.png "XMonad window manager with Rofi run prompt")

### Clone this repo in your local machine

```sh
mkdir ~/Documents/dotfiles_gitlab
echo "alias gitbare='/usr/bin/git --git-dir=$HOME/Documents/dotfiles_gitlab --work-tree=$HOME '" >> ~/.bashrc
source ~/.bashrc
git config --global init.defaultBranch main
```
Then cd into $HOME
```sh
echo "dotfiles_gitlab" >> .gitignore
```
Now, for SSH usage
```sh
git clone --bare git@gitlab.com:ArijitKHaldar/dotfiles_gitlab.git $HOME/Documents/dotfiles_gitlab
```
Or, with HTTPS
```sh
git clone --bare https://gitlab.com/ArijitKHaldar/dotfiles_gitlab.git $HOME/Documents/dotfiles_gitlab
```
Then, to prevent other files from HOME directory to be tracked
```sh
gitbare config --local status.showUntrackedFiles no
```
```sh
gitbare push --set-upstream origin main
```
Now, check if gitbare pull works, else
```sh
gitbare pull origin main --allow-unrelated-histories
```
The files not already in home directory may show as deleted, if gitbare status is checked, then
```sh
gitbare restore --staged .
```
Then gitbare restore each file from remote repo to let those files appear in your HOME directory
